package com.epam.rd.autotasks;

public class FunctionsTask2 {
    /**
     * <summary>
     * Implement code according to description of task.
     * </summary>
     * if set invalid arguments in method, then method must throws
     * IllegalArgumentException
     */
    public static boolean isSorted(int[] array, SortOrder order) {
        //throw new UnsupportedOperationException();
        //This will depend on what you have to return for null condition
        if (array == null || array.length <= 1) {
            return true;
        }
        //Where the condition fpr sortOder is met,we find any element
        // which is greater then its next element and return false.
        // otherwise it will be descending.
        if (order == SortOrder.ASC) {
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    return false;
                }

            }
        } else {
            for (int i = 1; i < array.length; i++) {
                if (array[i] > array[i - 1]) {
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * <summary>
     * Implement code according to description of task.
     * </summary>
     * if set invalid arguments in method, then method must throws
     * IllegalArgumentException
     * @return
     */
    public static int[] transform(int[] array, SortOrder order) {
        //throw new UnsupportedOperationException();
        if (array == null || array.length <= 1 || array.length <= 0) {
            return array;
        }

        //Where the sortOrder condition is either Ascendinding or Descendition transform
        if (order == SortOrder.ASC || order == SortOrder.DESC) {
            for (int index = 0; index < array.length; index++) {
                //increase each array element by its index
                array[index] +=  index;
            }
        }
        return array;
    }
}



